import java.util.Scanner;


class Palindromo{
    public static void main(String[] args) {
        String nombre;
        char letra;
        StringBuilder sb = new StringBuilder();
        sb.append("");
        System.out.println("PALINDROMO");
        System.out.println("----------");
        System.out.println("Inserta una palabra: ");
        Scanner teclado = new Scanner(System.in);
        nombre = teclado.nextLine();
        char [] palabra = nombre.toCharArray();
        for(int i=palabra.length-1 ; i >= 0 ; i--){
           letra = palabra[i];
           sb.append(letra);
        }
        String result = sb.toString().toLowerCase();
        result = result.substring(0,1).toUpperCase() + result.substring(1);
        System.out.println(result);
        teclado.close();
    }
}