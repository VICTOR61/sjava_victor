import java.util.Random;
import java.util.Scanner;
class adivina{
    public static void main (String[] args){
        Scanner keyboard = new Scanner(System.in);
        int numero=0;
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        int contador=0;
        while (incognita != numero){
            System.out.printf("Adivina el numero... ");
            numero = keyboard.nextInt();
            contador=contador+1;
            if (incognita == numero){
                System.out.println("Enhorabuena, has adivinado el numero");
            }else if (contador>8){
                System.out.println("Lo siento, has perdido");
                break;  
            }
        }
        System.out.println("Has utilizado " + contador + " intento/s");
        if(contador>8){
            System.out.println(incognita + " Este era el numero...");
        }
               
    }
}