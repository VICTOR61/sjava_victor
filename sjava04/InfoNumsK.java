import java.util.Scanner;
public class InfoNumsK{
public static void main(String[] args) {
    
    Scanner keyboard = new Scanner(System.in);
    int total = 0;
    int num=1;
    int i;
    int mayor=0;
    int menor=9999;
    int contador=0;
    int media=0;
    do {
        System.out.printf("Entra un num: ");
    
        try {  
                num = keyboard.nextInt();
                contador=contador+1;
                if (num==0) continue;
                total += num; 
                if(num>mayor){
                    mayor=num;
                }
                if(num<menor){
                    menor=num;
                }
            } catch (Exception e) {  
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            } 
    } while (num>0);
    contador=contador-1;
    media=total/contador;
    System.out.println(contador + " números introducidos");
    System.out.println("La suma es "+ total);
    System.out.println("El mayor es "+ mayor);
    System.out.println("El menor es "+ menor);
    System.out.println("La media "+ media);
    keyboard.close();
}
}