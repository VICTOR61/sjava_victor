class Person {
    String nombre;
    int edad;

    public Person(String nombre,int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    public void comparaEdad(Person otra){
        if(this.edad > otra.edad){
            System.out.printf("%s es mayor que %s",this.nombre,otra.nombre);
        }else{
            System.out.printf("%s es mayor que %s",otra.nombre,this.nombre);
        }
    }
}
