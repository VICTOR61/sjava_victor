package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;

    public Moto(String marca, String modelo, int cc, int cv, int pvp) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
    }
    public void comparapvp(Moto otra){
        int a = otra.pvp - this.pvp;
        if (this.pvp > otra.pvp) {
        System.out.printf("La %s es %d EUR más cara que la %s", this.marca,a,otra.marca);
        } else {
            System.out.printf("La %s es %d EUR más más barata que la %s", otra.marca,a, this.marca);   
        }
    }

}