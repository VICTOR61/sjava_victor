class Hombre extends Persona {

    @Override
    public String getNombre(){
        return "el Sr. "+this.nombre;
    }

    public Hombre(String nombre, int edad) {
        super(nombre, edad);
    }

}